<?php

class Deven_Randmar_Model_Adapter_RandmarAdapter {

    const RANDMAR_URL_BASE = 'https://api.randmar.com/';

    private $userName;
    private $password;
    private $debug;
    private $defaultPictureUrl = "https://pgtech.ca/logo_pgtech_no_data.jpg";

    public function __construct($userName, $password, $debug = false) {
        $this->userName = $userName;
        $this->password = $password;
        $this->debug = $debug;
    }

    public function getManufacturers() {
        $manufacturersJSON = '[{"Id":1,"Name":"Brother"}, {"Id":8,"Name":"Canon"}, {"Id":2,"Name":"Epson"}, {"Id":3,"Name":"Hewlett-Packard"}, {"Id":4,"Name":"Lexmark"}, {"Id":5,"Name":"Okidata"}, {"Id":6,"Name":"Samsung"}, {"Id":7,"Name":"Xerox"}]';
        $manufacturers = json_decode($manufacturersJSON, false);

        return $this->adjustObject($manufacturers);
    }

    public function getManufacturerIdByName($name) {
        switch ($name) {
            case "Brother" :
                return 1;
            case  "Canon" :
                return 8;
            case "Epson":
                return 2;
            case "Hewlett-Packard" :
                return 3;
                break;
            case  "Lexmark" :
                return 4;
            case "Okidata":
                return 5;
            case "Samsung":
                return 6;
            case "Xerox":
                return 7;
        }
    }

    public function getPrintersByManufacturerId($manufacturerId) {
        $manufacturer = $this->retrieveDataFromResellerCartridgeFinderAPI('manufacturers', $manufacturerId);
        return $manufacturer;
    }

    public function  retrievePrinterById($printerId) {
        $printer = $this->retrieveDataFromResellerCartridgeFinderAPI('printer', $printerId);
        return $printer;
    }

    public function retrieveProductById($supplyId) {
        $supply = $this->retrieveDataFromResellerCartridgeFinderAPI('supply', $supplyId);
        return $supply;
    }

    public function retrievePrinterByTitle($printerTitle) {
        $printer = $this->retrieveDataFromResellerCartridgeFinderAPI('printer', $printerTitle);
        return $printer;
    }

    public function retrieveSupplyByMPN($supplyMPN) {
        $supply = $this->retrieveDataFromResellerCartridgeFinderAPI('supply/MPN', $supplyMPN);
        return $supply;
    }

    public function  searchByName($nameSearched) {
        $products = $this->retrieveDataFromResellerCartridgeFinderAPI('globalsearch', $nameSearched);
        return $products;
    }

    public function getPriceProfiles() {
        $priceProfiles = $this->retrieveDataFromResellerCartridgeFinderAPI('priceprofiles');
        return $priceProfiles;
    }

    private function retrieveDataFromResellerCartridgeFinderAPI($endpoint, $id = '') {

        $url = self::RANDMAR_URL_BASE . 'v1/resellercartridgefinder/' . $endpoint;

        if ($id != '') {
            $url .= '/' . $id;
        }

        $url .= '?username=' . $this->userName . '&password=' . $this->password;

        $info = $this->retrieveDataFromURL($url);
        $info = $this->adjustObject($info);

        if (isset($info) && isset($info->StatusCode) && $info->StatusCode == "200")
        {
            return $info->Content;
        }

        return null;
    }

    private function retrieveDataFromURL($url)
    {
        $url = str_replace (' ', '%20', $url);
        $url = strtolower($url);

        if ($this->debug)
        {
            echo $url . '<br />';
        }

        // http://stackoverflow.com/questions/16700960/how-to-use-curl-to-get-json-data-and-decode-the-data/16701318#16701318
        // Initiate curl
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // Disable SSL verification
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Will return the response, if false it print the response
        curl_setopt($ch, CURLOPT_URL, $url); // Set the url
        $result=curl_exec($ch);
        curl_close($ch);

        return json_decode($result, false);
    }

    private function adjustObject($oldObject)
    {
        try {

            $object = $oldObject;
            foreach ($object as $key => $value) {

                if (is_array ($value))
                {
                    $object->$key = $this->adjustObject($value);
                }
                else if (is_object($value))
                {
                    $adjustedObject = $this->adjustObject($value);
                    if(isset($object->$key)) {
                        $object->$key = $adjustedObject;
                    } else {
                        $object[$key] = $adjustedObject;
                    }
                }
                else if($key == "Picture" && is_null($value))
                {
                    $object->$key = $this->defaultPictureUrl;
                }
            }

            return $object;

        } catch (Exception $e) {
        }

        return $oldObject;
    }
}