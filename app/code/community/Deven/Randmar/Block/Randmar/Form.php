<?php

/**
 * Created by PhpStorm.
 * User: NN
 * Date: 1/16/2018
 * Time: 8:36 PM
 */
class Deven_Randmar_Block_Randmar_Form extends Deven_Randmar_Block_Randmar
{
    public function getSearchFormHtml()
    {
        $code = '';
        $code .= "\t<div id='randmar-locator' class='main-randmar-locator' siteurl='".$this->getBaseUrl()."'>\n";
        $code .= "<div class='overlay'><div class='loading'><img src='".$this->getSkinUrl('images/randmar/loading-2.gif')."'></div></div>";
        $code .= "\t<div class='locator-buttons-container'>\n";
        $code .= "\t<div class='drop same-row-left'style='display:none;'>&nbsp;</div>\n";
        $code .= "\t<div class='pgtech-button pink-button same-row-left'style='display:none;'>".__('Localisateur de cartouches d&#39;origines','randmar')."</div>\n";
        $code .= "\t<div id='search-by-number-button' class='pgtech-button blue-button same-row-right' style='display:none;'>".__('Par num&eacute;ro','randmar')."</div>\n";
        $code .= "\t<div id='search-by-brand-button' class='pgtech-button yellow-button same-row-right current-search' style='display:none;'>".__('Par marque','randmar')."</div>\n";
        $code .= "\t</div><!-- end of locator-buttons-container -->\n";
        $code .= "\t<div class='search-by-brand-container'>\n";
        $code .= "\t<div class='black-circle same-row-left'>1</div><div class='same-row-left'><font id='label-1' class='font-26 line-40 black bold'>".__('Choisissez votre marque','randmar')."</font></div>\n";
        $code .= "\t<div class='end-row'></div>\n";
        $code .= $this->getBrandLogoRows();
        $code .= "\t<div class='end-row'></div>\n";
        $code .= "\t<div class='black-circle same-row-left'>2</div><font id='label-2' class='font-26 line-40 black bold'>".__('Choisissez votre imprimante','randmar')."</font>\n";
        $code .= "\t<div class='end-row'></div>\n";
        $code .= "\t<select id='printer-select' name='printer-select' class='printer-select'>
				<option selected disabled hidden>".__('Choisissez votre imprimante','randmar')."</option>";
        $code .= "</select>\n";
        $code .= "\t<input id='cartridge-number' class='cartridge-number' value='' placeholder='".__('Tapez un num&eacute;ro de cartouche','randmar')."' />\n";
        $code .= "\t<div class='end-row'></div>\n";
        //$code .= "\t<div class='black-circle same-row-left'>3</div><div id='search-button' onclick=\"alert('bonjour');\" style='margin-bottom: 10px; width: 80%; background-color: #0092d5;' class='black-button same-row-left'>".__('LOCALISER','randmar')."</div>\n";
        //<a href="/search/" class="black-button same-row-left search-button-randmar" onclick="this.href = '/search/' + document.getElementById('search-input').value">Localiser</a>

        $code .= "\t<div class='black-circle same-row-left'>3</div><a class='black-button same-row-left' href='/Printer/' style='margin-bottom: 10px; width: 80%;' onclick='var e = document.getElementById(\"printer-select\"); var printer = e.options[e.selectedIndex].text;  pritnerRedirect(printer);'>".__('LOCALISER','randmar')."</a></div>\n";
        $code .= "\t<div class='end-row'></div>\n";
        $code .= "\t</div><!-- search-by-brand-container -->\n";
        $code .= "\t<div class='search-results-container'>\n";
        $code .= "\t</div><!-- search-results-container -->\n";
        $code .= "\t</div><!-- end of randmar-locator -->\n";

        $code .= $this->getStyles();

        return $code;
    }

}