<?php

/**
 * Created by PhpStorm.
 * User: Welcome
 * Date: 1/17/2018
 * Time: 3:56 PM
 */
class Deven_Randmar_Block_Randmar extends Mage_Core_Block_Template
{

    protected function _construct()
    {
        parent::_construct();
    }

    public function getRandmarAdapter()
    {
        return new Deven_Randmar_Model_Adapter_RandmarAdapter('170229', 'ttoner911', false);
    }

    public function getManufacturerImage($name)
    {
        return $this->getSkinUrl('images/randmar/logo_' . strtolower($name) . '.png');
    }

    public function getStyles()
    {
        return "<style>
.brand-logo {
	width: 25%;
	padding: 0 5%;
	text-align: center;
	float: left;
}
img {
    max-width: 100%;
    height: auto;
}
.brand-logo.selected,
#printer-header,
#printer-info {
	border: solid 2px #1589C6;
}
#printer-header,
#printer-header {
	padding: 2% 5%;
}
#printer-header {
	font-size: 2em;
	font-weight: bold;
}
#printer-info{
	min-height: 200px;
}
#printer-info,
#cartridge-info { 
	font-size:1.2em;
}
#printer-pic,
#printer-name,
#printer-properties,
#printer-count {
	width: 25%;
	float: left;
	font-size: 1em;
	text-align: center;
  	display: flex;
  	align-items: center;
  	min-height: 200px;
  	justify-content: center;
}
#printer-name-contents {
	display:block;
}
#cartridge-info{
	border: solid 3px #0A039E;
	min-height: 280px;
}
#cartridge-pic,
#cartridge-name,
#cartridge-title,
#cartridge-count {
	width: 25%;
	float: left;
	font-size: 1em;
	text-align: center;
  	display: flex;
  	align-items: center;
  	min-height: 280px;
  	justify-content: center;
}
#cartridge-name-contents {
	display:block;
}
.underline {
	text-decoration: underline;
}
.bolder {
	font-weight: bolder;
}
.light-blue-button {
	background-color: #1589C6;
}
.dark-blue-button {
	background-color: #0A039E;
}
.search-input {
    margin: 2px;
    font-size: 1.5em;
    width: 50%;
}
.search-button-randmar {
    margin-left: 25px;
    width: 45%;
}
/*** Hide title in printer/supply/search page ***/
#post-2712 h1.entry-title.main_title,
#post-2715 h1.entry-title.main_title,
#post-2710 h1.entry-title.main_title{
	display: none;
}

/*** Hide title in printer/supply/search english page ***/
#post-2720 h1.entry-title.main_title,
#post-2724 h1.entry-title.main_title,
#post-2722 h1.entry-title.main_title{
	display: none;
}


@media screen and (max-width: 800px) {
	.brand-logo {
		width: 50%;
	}
}

</style>";
    }

    public function getBrandLogoRows($manufacturer_selected="") {

        $html = "\t<div id='brands-logos' class='brands-logos'>\n";

        foreach($this->getRandmarAdapter()->getManufacturers() as $brand) {
            $class = $manufacturer_selected == $brand->Name? ' selected':'';
            $html .= "<div class='brand-logo".$class."' id='".$brand->Id."'><img class='img-logo ".$brand->Name."' id='".$brand->Id."' src='". $this->getManufacturerImage($brand->Name) . "' alt='".$brand->Name."'></div>";
        }

        $html .= "\t</div><!-- brands-logos -->\n";

        return $html;
    }
}