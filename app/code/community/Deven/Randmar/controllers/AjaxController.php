<?php

/**
 * Created by PhpStorm.
 * User: NN
 * Date: 1/14/2018
 * Time: 1:53 PM
 */
class Deven_Randmar_AjaxController extends Mage_Core_Controller_Front_Action
{
    public function localiseAction()
    {
        echo $this->listCartridges();
    }

    public function addAction()
    {
        $params = $this->getRequest()->getParams();

        $additional = array_merge(
            $params,
            array(Mage_Core_Model_Url::FORM_KEY => Mage::getSingleton('core/session')->getFormKey())
        );

        $cartModel = Mage::helper('checkout/cart')->getCart();

        $productModel = Mage::getModel('catalog/product');

        $product = $productModel->getIdBySku($params['id']);


        $titl = str_replace("_c_", "'", $params['t']);
        $titl = str_replace("_lp_", "(", $titl);
        $titl = str_replace("_rp_", ")", $titl);

        $price = str_replace("p",".",$params['pr']);
        $price = $price*1;

        if(!$product) {
            $productModel->setAttributeSetId(4)//ID of a attribute set named 'default'
            ->setWebsiteIds(Mage::app()->getWebsites())
            ->setTypeId('simple')//product type
            ->setCreatedAt(strtotime('now'))//product creation time
            ->setUpdatedAt(strtotime('now'))//product update time
            ->setSku($params['id'])//SKU
            ->setName($titl)//product name
            ->setStatus(1)//product status (1 - enabled, 2 - disabled)
            ->setTaxClassId(2)//tax class (0 - none, 1 - default, 2 - taxable, 4 - shipping)
            ->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH)//catalog and search visibility
            /*->setManufacturer(28)
            ->setColor(24)*/
            ->setWeight(0)
                ->setPrice($price)
                ->setMetaTitle($titl)
                ->setStockData(array(
                        'use_config_manage_stock' => 0, //'Use config settings' checkbox
                        'manage_stock' => 0, //manage stock
                    )
                );

            $this->_saveAPImage($productModel, $params['pic']);

            $product = $productModel->save();

            $cartModel->addProduct($product->getEntityId(), $additional);
            $cartModel->save();

        } else {
            $cartModel->addProduct($product, $additional);
            $cartModel->save();
        }

        return;

    }


    public function indexAction()
    {
        $randmarAdapter = new Deven_Randmar_Model_Adapter_RandmarAdapter('170229', 'ttoner911', false);

        $params = $this->getRequest()->getParams();

        switch ($params['a']) {
            case 'gpfbi':	//getPrintersFromBrandId
                $manufacturer = $randmarAdapter->getPrintersByManufacturerId($params['id']);
                $printers = $manufacturer->Printers;
                //print_r($printers);
                //print_r($lang);

                foreach ($printers as $printer) {
                    $result .= "\t<option value='$printer->Id'>";
                    $result .= /*$lang=='fr'? */$printer->TitleFr /*: $printer->TitleEn*/;
                    $result .= "</option>\n";
                }

                break;

            case 'gpsfpi':		//getPrinterSuppliesFromPrinterId
                $printer = $randmarAdapter->retrievePrinterById($params['id']);
                //$result .= $lang;
                $result .= listCartridges($printer->Supplies);
                break;

            case 'gpsfn':	//getPrinterSuppliesFromNumber
                $supplies = $randmarAdapter->searchByName($params['cn']);
                $cartidges = array();
                $supply_ids = array();
                foreach ($supplies as $supply) {
                    if($supply->ProductType != 'Supply' || in_array($supply->SupplyId, $supply_ids)) {
                        continue;
                    }
                    $supply_ids[] = $supply->SupplyId;
                    $cartidges[] = $randmarAdapter->retrieveProductById($supply->RandmarSKU);
                }
                $result .= listCartridges($cartidges);
                //print_r($supplies);
                break;
            case 'srch':	//Do a search

                $supplies = $randmarAdapter->searchByName($params['cn']);
                $cartridges = array();
                $supply_ids = array();
                $printers = "";
                $printers_ts = array();

                foreach ($supplies as $supply) {
                    if($supply->ProductType != 'Supply' || in_array($supply->SupplyId, $supply_ids)) {
                        continue;
                    }
                    $supply_ids[] = $supply->SupplyId;
                    $cartridges[] = $randmarAdapter->retrieveProductById($supply->RandmarSKU);
                    /*
                    if(strlen($supply->DescriptionEn)>0) {
                        //Look for the printers in the cartridges found
                        $printers_temp = split(',', trim(substr($supply->DescriptionEn, strpos($supply->DescriptionEn, ':')+1)));
                        //print_r($printers_temp);
                        //Add to the printers list those thar aren't already in the list
                        foreach ($printers_temp as $printer) {
                            $printer = trim($printer);
                            if( strpos($printer, $_REQUEST['cn'])!==false && !in_array($printer, $printers_ts)) {
                                $printers_ts[]=$printer;
                            }
                        }
                    }

                    //Also use the string received to searh if there exist printers with that name
                    if(strlen($supply->ManufacturerName)>0) {
                        if( !in_array($supply->ManufacturerName.'-'.$_REQUEST['cn'], $printers_ts)
                            && !in_array($supply->ManufacturerName.' '.$_REQUEST['cn'], $printers_ts)) {
                            $printers_ts[]=$supply->ManufacturerName.'-'.$_REQUEST['cn'];
                        }
                    }*/
                }
                /*
                //Do the actual printer search
                foreach ($printers_ts as $printer) {
                    $printer_s = $randmarAdapter->retrievePrinterByTitle(str_replace(' ','-',$printer));
                    //print_r($printer_s);
                    if($printer_s) {
                        $printers .= display_printer($printer_s,$lang);
                    }
                }

                $result .= $printers;
                */
                //$result .= display_cartridges($cartridges,$lang);
                $result = $this->listCartridges($cartridges);

                break;

            default:
                $result = "<div id='result' class='error'>error</div>";
                break;
        }

        echo $result;
    }

    protected function _saveAPImage($product, $imageUrl)
    {
        $image_type = 'jpg'; //find the image extension
        $filename   = md5($imageUrl).'.'.$image_type; //give a new name, you can modify as per your requirement

        if ( !file_exists(Mage::getBaseDir('media') . DS . 'import') ) {
            $oldmask = umask(0);  // helpful when used in linux server
            mkdir (Mage::getBaseDir('media') . DS . 'import', 0777);
        }

        $filepath   = Mage::getBaseDir('media') . DS . 'import'. DS . $filename; //path for temp storage folder: ./media/import/

        file_put_contents($filepath, file_get_contents(trim($imageUrl))); //store the image from external url to the temp storage folder
        $mediaAttribute = array (
            'thumbnail',
            'small_image',
            'image'
        );

        $product->setMediaGallery (array('images'=>array (), 'values'=>array ())) //media gallery initialization
        ->addImageToMediaGallery($filepath, $mediaAttribute, true, false); //assigning image, thumb and small image to media gallery
    }

    public function listCartridges($supplies) {

        $html = '';
        $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
        $tempurl = substr($_SERVER['REQUEST_URI'],0,strpos($_SERVER['REQUEST_URI'],'/plugins/randmar/')+17);
        //$url = $protocol . $_SERVER['HTTP_HOST'] . $tempurl . 'assets/imgs/epson_cartridge.png';
        $url = $protocol . $_SERVER['HTTP_HOST'] . $tempurl . 'assets/imgs/logo_pgtech_no_data.jpg';

        $randmarAdapter = new Deven_Randmar_Model_Adapter_RandmarAdapter('170229', 'ttoner911', false);

        $url_pic = '';
        $supply_ids = array();
        /*$options = get_option( 'randmar_options' );*/
        $profit_margin = Mage::getStoreConfig('randmar/default/default_profit')*1;
        //$profit_margin = 20;
        $profit_factor = 100/(100 - $profit_margin);

        foreach ($supplies as $supply) {
            //$title = trim($supply->DescriptionFr);
            //if(strlen($title)==0) $title = trim($supply->TitleFr);
            if($supply->ProductType != 'Supply' || in_array($supply->SupplyId, $supply_ids)) {
                continue;
            }
            $supply_ids[] = $supply->SupplyId;

            $manufacturerId = $randmarAdapter->getManufacturerIdByName($supply->ManufacturerName);

            //$supply = $randmarAdapter->retrieveProductById($supply->RandmarSKU);
            //$supply = $supply;
            //print_r($supply);
            /*if($options['profit_'.$supply->ManufacturerId] != "") {
                $profit_margin = $options['profit_'.$supply->ManufacturerId]*1;
                $profit_factor = 100/(100 - $profit_margin);
            }*/
            if(Mage::getStoreConfig('randmar/default/profit_'.$manufacturerId)!="") {
                $profit_margin = Mage::getStoreConfig('randmar/default/profit_'.$manufacturerId)*1;
                $profit_factor = 100/(100 - $profit_margin);
            }
            //print_r('Profit Margin: ' . $profit_margin);
            $title = trim($supply->TitleEn);
            $description = trim($supply->DescriptionEn);
            if(Mage::app()->getLocale()->getLocaleCode()=='fr_FR') {
                $title = trim($supply->TitleFr);
                $description = trim($supply->DescriptionFr);
            }
            $sup_pic = trim($supply->Picture);
            if(strlen($sup_pic)>0) $url_pic = $sup_pic;
            else $url_pic = $url;

            $html .= "<div class='cartridge-result supply-".$supply->SupplyId."'>
			<div class='cartridge-pic floating-div'>
			<a href='" . Mage::getBaseUrl() . 'randmar/index/supply/mpn/' . $supply->MPN . "/' >
				<img src='" . $url_pic . "'>
			</a>
			</div>
			<div class='catridge-info floating-div'>
			<a href='" . Mage::getBaseUrl() . 'randmar/index/supply/mpn/' . $supply->MPN . "/' >
			<div class='catridge-title'>".$title."</div></a>
			<div class='catridge-data'>".$supply->Technology."<br />".$supply->Color."<br />".$supply->PageYield . " Pages"."<!-- Article:$supply->SupplyId
				<ul>
				<li>Noire</li>
				<li>N° du fabricant: ".$supply->RandmarInfo->PartNumber."</li>
				<li>Description: ".$description."</li>
				</ul -->
			</div>
			</div>
			<div class='quantity-section floating-div'>
				<div class='stock-section'><br />
					<div class='stock-title'>".Mage::helper('deven_randmar')->__('Availability of stock')."</div><br />
					<div class='stock-info'>";

            $html_montreal = '';
            $html_2 = '';
            $count_disponibility = 0;

            if(is_array($supply->RandmarInfo->Warehouses)) {

                foreach ($supply->RandmarInfo->Warehouses as $warehouse) {
                    //if($warehouse->Quantity > 0) {
                    $count_disponibility += $warehouse->Quantity*1;
                    if(strcmp('MTL', trim($warehouse->Id)) == 0) {
                        $html_montreal = '<tr><td>' . $warehouse->Name . '</td><td>' . $warehouse->Quantity . '</td></tr>';
                    } else {
                        $html_2 .= '<tr><td>' . $warehouse->Name . '</td><td>' . $warehouse->Quantity . '</td></tr>';
                    }
                    //}
                }
            }

            //if($count_disponibility > 0)
            $html .= str_replace('Randmar ', '', '<table>' . $html_montreal . $html_2 . '</table>');
            //else
            //	$html .= $translations[$lang]['unavailable'] . '<br>'; //Currently not stock available
            $html .= "</div>
						</div>
			</div>
			<div class='cart-section floating-div'><br />
			<div class='price'>".number_format(round($supply->RandmarInfo->Price*$profit_factor,2),2)."$</div>
			<input name='quantity' class='quantity-input' type='number' min='0' step='1' value='1'>
			<div id='".$supply->SupplyId."' class='add-button'>".Mage::helper('deven_randmar')->__('Add to cart')."</div>
			</div>
		</div><!-- cartridge-result -->
		<hr class='cartridge-result-rule'>
		";
        }

        if(strcmp('', $html) == 0) {
            $html = "<br/><hr class='cartridge-result-rule'>
		<div class='no-results'>" . Mage::helper('deven_randmar')->__('No cartridges found') . "</div>
		<hr class='cartridge-result-rule'><br/>"; //Not cartridges found
        }

        return $html;
    }

}