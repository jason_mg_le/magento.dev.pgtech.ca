<?php

/**
 * Created by PhpStorm.
 * User: NN
 * Date: 1/14/2018
 * Time: 4:08 PM
 */
class Deven_Randmar_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        $this->loadLayout();
        $this->getLayout()->getBlock('localiseForm');
        $this->renderLayout();
    }

    public function searchAction()
    {
        $this->loadLayout();
        $this->getLayout()->getBlock('searchForm');
        $this->renderLayout();
    }

    public function printerAction()
    {
        $this->loadLayout();
        $this->getLayout()->getBlock('printerPage')->setData('printerTitle', $this->getRequest()->getParam('title'));
        $this->getLayout()->getBlock('cartridgesPage')->setData('printerTitle', $this->getRequest()->getParam('title'));
        $this->renderLayout();
    }

    public function supplyAction()
    {
        $this->loadLayout();
        $this->getLayout()->getBlock('supplyPage')->setData('mpn', $this->getRequest()->getParam('mpn'));
        $this->renderLayout();
    }

}