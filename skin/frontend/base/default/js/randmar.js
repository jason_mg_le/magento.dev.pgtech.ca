var printers_by_brand = [];

jQuery(document).ready(function($) {
	//alert('Hi from Ramdar js');

	if($('.brands-logos').length>0 && $('.main-randmar-locator').length>0){
		addClickListenerToBrands();
		$('.brand-logo').css('cursor','pointer');

		//Preselect first brand
		$('.brand-logo').first().trigger('click');
	}

	if($('.printer-brands').length>0) {

		$('#left-arrow').click(function(event){
			temp = $('.brands').first().wrapAll('<div>').parent().html();
			$('.brands').first().parent().remove();
			$('.logos-container').append(temp);
			addClickListenerToBrands();
		});

		$('#right-arrow').click(function(event){
			temp = $('.brands').last().wrapAll('<div>').parent().html();
			$('.brands').last().parent().remove();
			$('.logos-container').prepend(temp);
			addClickListenerToBrands();
		});

		$('body').keypress(function (e) {
			var key = e.which;
			/*if(key == 13) { // the enter key code
				$('#search-button').trigger('click');
				return;
			}*/

			if(key == 37) {	//Left key

			}

			if(key == 39) {	//Right key

				i = $('.brands').index($('.brands.selected'));
				max = $('.brands').length - 1;
				if(++i >= max) i = 0;
				$('.brands')[i].trigger('click');
			}
		});

		$('.pgtech-button.blue-button').click(function(event) {
		});

		addClickListenerToBrands();

		//Preselect first brand
		$('.brands').first().trigger('click');
	}	//if printer-brands

	if($('.main-randmar-locator').length>0) {

		$('#search-by-brand-button').click(function(event){
			$('.search-by-brand-container').show();
			$('#search-by-brand-button,#search-by-number-button').removeClass('current-search');
			$('#search-by-brand-button').addClass('current-search');
			$('#label-2').html('Choisissez votre imprimante');
			$('.printer-select, .printer-brands, .brands-logos, #label-1').show();
			$('.black-circle').css('background','black');
			$('.cartridge-number').hide();
			hideLoading();
			$('.search-results-container').html('');
		});

		$('#search-by-number-button').click(function(event){
			$('.search-by-brand-container').show();
			$('#search-by-brand-button,#search-by-number-button').removeClass('current-search');
			$('#search-by-number-button').addClass('current-search');
			/*if(lang == 'fr') {
				$('#label-2').html('Tapez un numéro de cartouche');
			} else {
				$('#label-2').html('Type a cartridge number');
			}*/
			$('#label-2').html($('.cartridge-number').attr('placeholder'));
			$('.printer-select, .printer-brands, .brands-logos, #label-1').hide();
			$('.black-circle').css('background','white');
			$('.cartridge-number').show();
			hideLoading();
			$('.search-results-container').html('');
		});

		$('#search-button').click(function() {
			u = $('#randmar-locator').attr('siteurl') + '/randmar/ajax/index';
			u += '?l=randmar';
			u += $('#search-by-brand-button').hasClass('current-search')? '&a=gpsfpi' : '&a=gpsfn';
			u += '&cn='+$('.cartridge-number').val();
			u += '&langu='+$('html').attr('lang').substr(0,2);
			u += '&id='+$('.printer-select').val();
			showLoading();
			$.ajax({
				url: u,
				method: "POST",
				success: function(result){
					$('.search-results-container').html(result);
					$('.search-by-brand-container').hide();
					hideLoading();
					addClickListenerToAddButton();
					addEnterListenerToQtyInput();
				},
				error: function(result) {
					hideLoading();
				}
			});
		});

		$('.cartridge-number').keypress(function (e) {
			var key = e.which;
			if(key == 13) { // the enter key code
				$('#search-button').trigger('click');
				return;
			}
		});
	}

	if($('.search-input').length>0){
		$('.search-input').keypress(function (e) {
			var key = e.which;
			if(key == 13) { // the enter key code
				$('#search-button').trigger('click');
				return;
			}
		});
	}

	if($('.et-cart-info').length > 0) {
		displayShoppingCartQty();
	}

	$( document ).ajaxComplete(function(event, xhr, settings) {
		if(!settings.url.includes('randmar/ajax/index?l=randmar'))
			displayShoppingCartQty();
		if(settings.url.includes('randmar/ajax/index?l=randmar&a=aptsc'))
			displayShoppingCartQty();
		//console(event);
	});

	//Translations
	/*lng = $('html').attr('lang').substr(0,2);
	if(lng == 'fr') {
		if($('#woo_pp_ec_button img').length>0) {
			$('#woo_pp_ec_button img').attr('src',$('#my-header .locator-buttons-container').attr('siteurl')+'/wp-content/plugins/randmar/assets/imgs/paypal-fr.png');
		}
	}
	if(lng == 'en'){
		if($('.woocommerce-checkout-payment').length>0){
			$('.payment_box.payment_method_paypal p').html('Pay with PayPal, you can pay with your credit card if you do not have a PayPal account.');
			$('.payment_method_stripe > label').html($('.payment_method_stripe > label').html().replace('Carte de crédit','Credit Card'));
		}
	}

	$('#ship-to-different-address').prepend('<input class="input-checkbox" id="other-shipping-addr" type="checkbox" name="other-shipping-addr">');
	$('#ship-to-different-address label').click(function(event) {
		$('#other-shipping-addr').prop("checked", !$('#other-shipping-addr').prop("checked") );
	});*/

	if($('.add-button').length>0){
		addClickListenerToAddButton();
	}

	//fixTranslationLinks();

	//if($('.main-randmar-locator').length>0) {
	//	window.history.forward(1);
	//}



function showLoading() {
	//$('.overlay').css('height','80%');
	rlh = $('#randmar-locator').outerHeight();
	rlw = $('#randmar-locator').outerWidth();
	lh = $('.loading').outerHeight();
	lw = $('.loading').outerWidth();
	ltm = (rlh - lh)/2;
	llm = (rlw -lw)/2;
	$('.overlay').css('height',rlh);
	$('.loading').css('margin-top',ltm);
	$('.loading').css('margin-left',llm);
}

function hideLoading() {
	$('.overlay').css('height','0px');
}

function addClickListenerToAddButton() {
	$('.add-button').click(function(event) {
		result = '.supply-'+$(this).attr('id');
		pic = $('.supply-'+$(this).attr('id')+' .cartridge-pic img').attr('src');
		siteu = $('#randmar-locator').attr('siteurl');
		/*if(pic.indexOf(siteu)>=0) {
			pic = pic.replace(siteu, '_siteu_');
		} else {
			pic = pic.replace('http','_h_');
		}*/
		//alert(result);
		u = $('#randmar-locator').attr('siteurl') + 'randmar/ajax/add';
		u += '?id='+$(this).attr('id');
		u += '&qty='+$(result+' .quantity-input').val();
		u += '&pr='+$(result+' .price').html().replace('$','').replace('.','p');
		u += '&t='+$(result+' .catridge-title').html();
		u += '&pic='+encodeURIComponent(pic);
		u = u.replace(/'/g,"_c_");
		u = u.replace(/\(/g,"_lp_");
		u = u.replace(/\)/g,"_rp_");
		//alert(u);
		//return;
		$.ajax({
			url: u,
			method: "POST",
			success: function(result) {
				$('body').append('<div class="pgtech-popup">Catridge added to shopping cart</div>');
				$('.pgtech-popup').slideDown("slow").delay(1500).fadeOut('slow');
                //console.log(result);
				location.reload();
				/*displayShoppingCartQty();*/
				/*if($('#sc-quantity').length > 0){
					$('#sc-quantity').html($('#sc-quantity').html()*1+1);
				}else{
					$('.et-cart-info').append('<div id="sc-quantity" class="blue-circle">1</div>');
				}*/
				//Go to shopping cart
				//window.location.href = $('a.et-cart-info').attr('href');
			},
			error: function(result) {
				hideLoading();
			}
		});
	});
}

function addEnterListenerToQtyInput() {
	$('.quantity-input').keypress(function (e) {
		var key = e.which;
		var supply_id = '';
		if(key == 13) { // the enter key code
			classes = $(this).parent().parent().attr('class').split(" ");

			for (index = 0; index < classes.length; index++) {
				//console.log(a[index]);
				if(classes[index].includes('supply')) {
					supply_id = classes[index].substr(7);
					break;
				}
			}

			$('#'+supply_id).trigger('click');

    		return;
    	}
    });
}

function addClickListenerToBrands() {
	$('.brands,.brand-logo').off('click');
	$('.brands,.brand-logo').click(function(event, origin) {

		id = $(this).attr('id');

		if(origin != "getAllBrandsPrinters") {
			$('.brands,.brand-logo').removeClass('selected');
			$(this).addClass('selected');

			if(printers_by_brand[id] !== undefined) {
				$('#printer-select').html(printers_by_brand[id]);
				return;
			}
		}

		u = $('#randmar-locator').attr('siteurl') + 'randmar/ajax/index';
		u += '?l=randmar';
		u += '&a=gpfbi';
		/*u += '&langu='+$('html').attr('lang').substr(0,2);*/
		u += '&id='+id;
		$.ajax({
			url: u,
			method: "POST",
			success: function(result, textStatus, jqXHR) {
				if( origin != "getAllBrandsPrinters" ) {
					$('#printer-select').html(result);
				}
				printers_by_brand[id] = result;
				getAllBrandsPrinters();
			}
		});
	});
}

function getAllBrandsPrinters() {

	brands = $('.brands');
	if(brands.length == 0) brands = $('.brand-logo');

	for (i = 0; i < brands.length; i++) {
		//bid = $($('.brands')[i]).attr('id');
		bid = $(brands[i]).attr('id');
		if(printers_by_brand[bid] === undefined) {
			//$($('.brands')[i]).trigger('click',["getAllBrandsPrinters"]);
			$(brands[i]).trigger('click',["getAllBrandsPrinters"]);
			break;
		}
	}

}

function displayShoppingCartQty() {
	u = $('#my-header .locator-buttons-container').attr('siteurl') + 'randmar/ajax/index';
	u += '?l=randmar';
	u += '&a=gsq';
	//u += '&langu='+$('html').attr('lang').substr(0,2);
	$.ajax({
		url: u,
		method: "POST",
		success: function(result) {
			if($('#sc-quantity').length > 0){
				$('#sc-quantity').html(result);
			} else {
				$('.et-cart-info').append('<div id="sc-quantity" class="blue-circle">'+result+'</div>');
			}
		}
	});
}

function fixTranslationLinks() {

	lng = $('html').attr('lang').substr(0,2);
	curr_url = window.location+"";

	//If printer page
	if(curr_url.indexOf('/printer/')>=0) {
		if(lng == 'en') {
			url_fr = curr_url.replace('/en/','/');
			$('.wpml-ls-item-en a').attr('href',curr_url);
			$('.wpml-ls-item-fr a').attr('href',url_fr);
		}
		if(lng == 'fr') {
			url_en = curr_url.replace('/printer/','/en/printer/')
			$('.wpml-ls-item-fr a').attr('href',curr_url);
			$('.wpml-ls-item-en a').attr('href',url_en);
		}
	}

	//If supply page
	if(curr_url.indexOf('/supply/')>=0) {
		if(lng == 'en') {
			url_fr = curr_url.replace('/en/','/');
			$('.wpml-ls-item-en a').attr('href',curr_url);
			$('.wpml-ls-item-fr a').attr('href',url_fr);
		}
		if(lng == 'fr') {
			url_en = curr_url.replace('/supply/','/en/supply/')
			$('.wpml-ls-item-fr a').attr('href',curr_url);
			$('.wpml-ls-item-en a').attr('href',url_en);
		}
	}

	//If search page
	if(curr_url.indexOf('/search/')>=0) {
		if(lng == 'en') {
			url_fr = curr_url.replace('/en/','/');
			$('.wpml-ls-item-en a').attr('href',curr_url);
			$('.wpml-ls-item-fr a').attr('href',url_fr);
		}
		if(lng == 'fr') {
			url_en = curr_url.replace('/search/','/en/search/')
			$('.wpml-ls-item-fr a').attr('href',curr_url);
			$('.wpml-ls-item-en a').attr('href',url_en);
		}
	}

}

function changeTranslationLinksOnSearch() {

	to_s = $('.search-input').val();
	lng = $('html').attr('lang').substr(0,2);
	curr_url = window.location+"";

	if(curr_url.indexOf('/search-randmar/')>=0) {
		curr_url = curr_url.replace('/search-randmar/','/search/');
	}

	//If search page
	if(curr_url.indexOf('/search/')>=0) {
		new_url = curr_url.substr(0, curr_url.indexOf('/search/')+8)+to_s+'/';
		if(lng == 'en') {
			url_fr = new_url.replace('/en/','/');
			$('.wpml-ls-item-en a').attr('href',new_url);
			$('.wpml-ls-item-fr a').attr('href',url_fr);
		}
		if(lng == 'fr') {
			url_en = new_url.replace('/search/','/en/search/')
			$('.wpml-ls-item-fr a').attr('href',new_url);
			$('.wpml-ls-item-en a').attr('href',url_en);
		}
	}

}

function searchRedirect(){
	new_url = $('#randmar-locator').attr('siteurl') + '/search/' + $('#search-input').val() + '/';
	//alert(new_url);
	event.preventDefault();
	$('#search-a').attr('href',new_url);
	window.location = new_url;
}



});

function printerRedirect(printerName){
    new_url = jQuery('#randmar-locator').attr('siteurl') + 'randmar/index/printer/title/' + printerName.replace(new RegExp(' ', 'g'), '-') + '/';
    //alert(new_url);
    event.preventDefault();
    jQuery('#search-a').attr('href',new_url);
    window.location = new_url;
}